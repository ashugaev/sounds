/**
 * Timer functon wrapper
 */
const timerAsync = async (label, callback) => {
  console.time(label);
  const res = await callback();
  console.timeEnd(label);
  return res;
};

const timer = (label, callback) => {
  console.time(label);
  const res = callback();
  console.timeEnd(label);
  return res;
};

module.exports = { timer, timerAsync };
