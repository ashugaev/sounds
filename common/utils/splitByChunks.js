const splitByChunks = ({ chunkSize, arr }) => {
  const chunks = [];
  for (let i = 0; i < arr.length; i += chunkSize) {
    const chunk = arr.slice(i, i + chunkSize);
    chunks.push(chunk);
  }

  return chunks;
};

module.exports = {
  splitByChunks,
};
