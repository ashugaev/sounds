// pages
export const homePath = '/home';
export const searchPath = '/search';
export const albumsPath = '/albums';
export const livePath = '/live';
export const autoTagPath = '/auto-tag';

// endpoints
export const tracksPath = '/api/tracks';
export const categoryBlocksPath = '/api/category_blocks';

// query params
export const searchQuery = 'search';
