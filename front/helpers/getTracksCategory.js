import { homePath } from 'constants';

export function getTracksCategory(pathname) {
  if (pathname.includes(homePath)) {
    return pathname.match(/\/(\w+)$/)[1];
  }
}
