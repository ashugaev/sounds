import {
  observable, action, runInAction,
} from 'mobx';
import axious from 'axios';
import {
  autoTagsPath,
} from '../../server/helpers/constants';

class AtoTagListStore {
    @observable items = [];

    @observable pending = true;

    @observable loaded = false;

    @observable hasError = false;

    @action.bound fetch(params) {
      const {
        callback, callbackArgs,
        fromObjId,
        afterObjId,
        beforeObjId,
        limit,
      } = params || {};

      this.pending = true;

      axious.get(autoTagsPath, {
        params: {
          fromObjId,
          afterObjId,
          beforeObjId,
          limit,
        },
      })
        .then(({ data }) => runInAction(() => {
          console.log('data', data);
          this.items = data;
          this.loaded = true;

          callback && callback(data, callbackArgs);
        }))
        .catch((err) => {
          console.log(err);
          this.hasError = true;
        })
        .finally(() => runInAction(() => {
          this.pending = false;
        }));
    }

    @action.bound byId(id, params) {
      const {
        callback, callbackArgs,
      } = params || {};

      this.pending = true;

      axious.get(`${autoTagsPath}/${id}`)
        .then(({ data }) => runInAction(() => {
          this.items = data;
          this.loaded = true;

          callback && callback(data, callbackArgs);
        }))
        .catch((err) => {
          console.log(err);
          this.hasError = true;
        })
        .finally(() => runInAction(() => {
          this.pending = false;
        }));
    }

    @action.bound resetItems() {
      this.items.clear();
    }
}

export default new AtoTagListStore();
