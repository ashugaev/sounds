import {
  observable, action, runInAction,
} from 'mobx';
import axios from 'axios';
import { TracksStoreCommon } from './utils/tracks-store-common';
import { autoTagTracksPath } from '../../server/helpers/constants';

class AutoTagTracksStore extends TracksStoreCommon {
    @observable isTracksLoading = false

    @observable isTracksLoaded = false

    @observable noTracksToFetch = false

    @observable tagId = null

    constructor() {
      super();

      this.limit = 30;
    }

    @action.bound
    fetch({
      tagId,
      rewrite,
      fromObjId,
      afterObjId,
      callback,
      callbackArgs,
      resetBefore,
    }) {
      if (tagId) {
        this.tagId = tagId;
      }

      if (this.isTracksLoading || this.noTracksToFetch) return;

      if (resetBefore) this.resetTracks();

      this.isTracksLoading = true;

      axios.get(autoTagTracksPath, {
        params: {
          tagId: this.tagId,
          afterObjId,
        },
      })
        .then(({ data }) => runInAction(async () => {
          if (!data.length) {
            this.noTracksToFetch = true;
          } else if (rewrite) {
            this.tracks.replace(data);
          } else {
            this.tracks.push(...data);
          }

          callback && callback(callbackArgs);

          this.isTracksLoaded = true;
        }))
        .catch((err) => {
          console.error(err);
        })
        .finally(() => runInAction(() => {
          this.isTracksLoading = false;
        }));
    }

    @action.bound
    resetMetaData() {
      this.isTracksLoading = false;
      this.noTracksToFetch = false;
    }

    @action.bound
    resetAppData() {
      this.resetTracks();
    }

    @action.bound
    setTagId(tagId) {
      this.tagid = tagId;
    }
}

export default new AutoTagTracksStore();
