import { defineConfig } from 'vite';

const path = require('path');

export default defineConfig({
  resolve: {
    alias: [
      { find: '@', replacement: path.resolve(__dirname, 'src') },
    ],
  },
  build: {
    sourcemap: true,
  },
  server: {
    proxy: {
      '/api': 'http://localhost:3000',
    },
  },
  css: {
    modules: {
      localsConvention: 'camelCase',
    },
  },
});
