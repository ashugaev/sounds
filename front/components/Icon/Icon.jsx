import React from 'react';
import cn from 'classnames';
import s from './Icon.sass';

const Icon = ({
  size, className, icon, color, isActive, opacity,
}) => {
  const iconClassname = cn(s.Icon, className, {
    [s['Icon_size_' + size]]: size,
    [s['Icon_icon_' + icon + (isActive ? '_isActive' : '')]]: icon,
    [s['Icon_color_' + color]]: color,
    [s['Icon_opacity_' + opacity]]: opacity,
  });

  return (
    <div className={iconClassname} />
  );
};

Icon.defaultProps = {
  size: 'm',
  color: 'white',
};

export default React.memo(Icon);
