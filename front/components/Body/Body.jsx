import React, { useEffect } from 'react';
import {
  Route, Switch, withRouter, useHistory,
} from 'react-router-dom';
import cn from 'classnames';
import PlayerBox from 'c/PlayerBox';
import Header from 'c/Header';
import OneChannel from 'p/OneChannel';
import OneCategory from 'p/OneCategory';
import Live from 'p/Live';
import Search from 'p/Search';
import Channels from '../../pages/Channels';
import ChannelWallpaper from '../ChannelWallpaper';
import Categories from '../../pages/Categories';
import {
  albumsPath, homePath, livePath, searchPath, autoTagPath,
} from '../../constants/index';
import s from './Body.sass';
import ItemEditModal from '../ItemEditModal.js';
import AutoTag from '../../pages/Autotag';


const Body = () => {
  const history = useHistory();

  useEffect(() => {
    if (history.location.pathname === '/') {
      history.push(homePath);
    }
  }, []);

  return (
    <div className={s.Body}>
      <Route exact path={`${albumsPath}/:id`} component={ChannelWallpaper} />
      <Header />
      <div className={cn(s.Content, { [s.Content_withNotifier]: false, [s.Content_withPlayer]: true })}>
        <Switch>
          <Route exact path={homePath} component={Categories} />
          <Route exact path={`${homePath}/:name`} component={OneCategory} />
          <Route exact path={albumsPath} component={Channels} />
          <Route exact path={`${albumsPath}/:id`} component={OneChannel} />
          <Route exact path={livePath} component={Live} />
          <Route exact path={searchPath} component={Search} />
          <Route exact path={`${autoTagPath}/:autotagId`} component={AutoTag} />
        </Switch>
        <PlayerBox className={s.PlayerBox} />
      </div>
      <ItemEditModal />
    </div>
  );
};

export default withRouter(Body);
