import React from 'react';
import get from 'lodash-es/get';
import Text from 'c/Text';
import Loader from 'c/Loader';
import LazyLoader from 'c/LazyLoader';
import { inject, observer } from 'mobx-react';
import s from './ItemsBlock.sass';

/**
 * Stupid version of ItemsBlock
 */
const ItemsBlock = inject('pageStore')(observer(({
  pageStore,
  title,
  lazy,
  children,
  isTracksLoading,
  noTracksToFetch,
  lazyLoadingHandler,
}) => {
  const {
  } = pageStore;

  return (
    <>
      {lazy && (
        <LazyLoader
          loadHandler={lazyLoadingHandler}
          pixelsLeftToLoad={600}
          skipLoads={isTracksLoading || noTracksToFetch}
        />
      )}
      <div className={s.ItemsBlock}>
        {title ? (
          <Text
            text={title}
            size="xl"
            className={s.Title}
            line="normal"
            bold
            cropLine
          />
        ) : (
          <div className={s.UpperPlaceholder} />
        )}
        <div className={s.List}>
          { children }
        </div>
      </div>
      {lazy ? (
        isTracksLoading ? <Loader /> : <div className={s.LoaderPlaceholder} />
      ) : (
        <div className={s.BottomPlaceholder} />
      )}
    </>
  );
}));

export default ItemsBlock;
