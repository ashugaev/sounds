import React, { useEffect, useRef } from 'react';
import YouTubePlayer from 'youtube-player';
import { observer, inject } from 'mobx-react';
import { withRouter } from 'react-router';
import get from 'lodash-es/get';
import query from 'query';
import { setTrackToLocalStorage } from '../../helpers/lastTrackNotifier';
import classes from './VideoPlayer.sass';

let player;

const playerStates = {
  UHSTARTED: -1, // Initial state
  ENDED: 0,
  PLAYING: 1,
  PAUSED: 2,
  BUFFERING: 3,
};

const VideoPlayer = inject('playerStore', 'playerTracksStore', 'tagsStore', 'pageStore')(observer(({
  playerStore,
  playerTracksStore,
  tagsStore,
  videoId,
  isPlaying,
  history,
  pageStore,
  isVisible,
}) => {
  const {
    newTimeValue, toggleIsPlaying,
  } = playerStore;
  const {
    onNextClick, currentTrack, changeTrigger,
  } = playerTracksStore;
  const { fetchTagsByIds } = tagsStore;
  const { setTags } = pageStore;

  useEffect(() => {
    player = YouTubePlayer('player', {
      playerVars: {
        autoplay: 0,
        rel: 0,
        disablekb: 1,
        controls: 0, // Hide video controls
        fs: 1, // Allow fullscreen
        modestbranding: 1, // Hide YouTube logo
      },
    });

    player.unMute();
    player.setVolume(50);

    player.on('stateChange', handlePlayerStateChange);
    player.on('ready', handlePlayerReady);
    player.on('playerReady', handlePlayerReady);
    player.on('pause');

    updateTagsFromQueries();
  }, []);

  useEffect(() => {
    if (videoId) {
      player.loadVideoById(videoId).then(() => {
        // фиксит автоплей при переключении следующего трека
        isPlaying ? player.playVideo() : player.pauseVideo();
      });

      if (currentTrack.tags && !currentTrack.tagsIsLoaded) fetchTagsByIds(currentTrack.tags, setTags);

      updateTrackQuery();

      setTrackToLocalStorage(get(currentTrack, 'snippet.title'), videoId, currentTrack._id);
    }
  }, [videoId, changeTrigger]);

  useEffect(() => {
    isPlaying ? (
      player.playVideo()
    ) : (
      player.pauseVideo()
    );
  }, [isPlaying]);

  const playerRef = useRef(null);

  useEffect(() => {
    const playerEl = document.getElementById('player');
    if (playerEl) {
      playerEl.style.display = isVisible ? 'block' : 'none';
    }
  }, [isVisible]);

  useEffect(() => {
    // Hack. Video stuck on endless loading by default.
    // Also player trigger pause event everytime when we change id.
    setInterval(async () => {
      const state = await player.getPlayerState();
      if (state === playerStates.UHSTARTED) {
        console.log('state', state);

        player.playVideo();
      }
    }, 1000);
  }, []);

  function handlePlayerStateChange({ data }) {
    if (data === playerStates.PLAYING) {
      console.log('PLAYING');
      toggleIsPlaying(true);
    } else if (data === playerStates.ENDED) {
      onNextClick();
    } else if (data === playerStates.PAUSED) {
      console.log('PAUSED');
      toggleIsPlaying(false);
    } else if (data === playerStates.BUFFERING) {
      console.log('BUFFERING');
    } else if (data === playerStates.UHSTARTED) {
      console.log('UHSTARTED');
    } else {
      console.log('player state changed', data);
    }

    setTime();
  }

  function handlePlayerReady(data) {
    console.info('ready', data);
  }

  function updateTrackQuery() {
    query.set(history, 'trackId', videoId);
    query.set(history, 'trackObjId', currentTrack._id);
  }

  // TODO: Переписать логику тегов
  function updateTagsFromQueries() {
    // const { tags } = qs.parse(get(history, 'location.search'));

    // if (!tags) return;

    // setFilterTags(tags.split(','), history, true);
  }

  async function setTime() {
    const { setCurrentTime, setDuration } = playerStore;

    const cur = await player.getCurrentTime();
    const dur = await player.getDuration();

    setDuration(dur);
    setCurrentTime(cur);
  }


  /**
     * Устанавливает время проигрывания трега
     */
  useEffect(() => {
    player.seekTo(newTimeValue);
  }, [newTimeValue]);

  return (
    <div
      ref={playerRef}
      className={classes.videoPlayer}
      id="player"
    />
  );
}));

export default withRouter(VideoPlayer);
