import React, { useEffect } from 'react';
import { inject, observer } from 'mobx-react';
import Loader from 'c/Loader';
import GenreItem from 'c/CategoryItem';
import { useHistory } from 'react-router-dom';
import query from 'helpers/query';
import ItemsBlock from '../../components/ItemsBlock';
import TagButton from '../../components/TagButton';
import { autoTagPath } from '../../constants';
import { capitalizeWords } from '../../../common/utils/capitalizeWords';

const Categories = inject('categoriesStore', 'categoryBlocksStore', 'autoTagListStore')(observer(({
  className,
  categoriesStore,
  categoryBlocksStore,
  autoTagListStore,
}) => {
  const {
    fetchCategories,
    categoriesLoading,
    getCategoriesByBlockId,
  } = categoriesStore;
  const {
    fetchCategoryBlocks,
    categoryBlocks,
  } = categoryBlocksStore;

  const history = useHistory();

  useEffect(() => {
    // Для ленивой загрузки в дальнейшем можно эту ф-цию перевызывать добавив параметр fromCategory для fetchCategoryBlocks
    (async () => {
      await fetchCategoryBlocks();

      const categoryBlockIds = categoryBlocks.map(el => el._id);

      // достанем айдишники
      await fetchCategories({ rewrite: true, categoryBlockIds });
    })();

    // autoTagListStore.fetch();

    return () => {
      // autoTagListStore.resetItems();
    };
  }, []);

  if (
    categoriesLoading
  // || !autoTagListStore.loaded
  ) {
    return <Loader />;
  }

  return (
    <div className={className}>
      {/* TODO: Release. Waits player refactor of fixes */}
      {/* TODO: Add question mark whick will explain what does it mean "Autotags" */}
      {/* <ItemsBlock */}
      {/*  title="Autotags" */}
      {/*  flexList */}
      {/*  centered */}
      {/*  smallGap */}
      {/* > */}
      {/*  { */}
      {/*    autoTagListStore.items.map(tag => ( */}
      {/*      <TagButton */}
      {/*        key={tag._id} */}
      {/*        text={capitalizeWords(tag.text)} */}
      {/*        theme="miniLabel" */}
      {/*        onClick={() => { */}
      {/*          history.push({ */}
      {/*            pathname: `${autoTagPath}/${tag._id}`, */}
      {/*            search: query.getString(history), */}
      {/*          }); */}
      {/*        }} */}
      {/*      /> */}
      {/*    )) */}
      {/*  } */}
      {/* </ItemsBlock> */}
      {categoryBlocks.map(({ _id, title }) => {
        const categories = getCategoriesByBlockId(_id);

        return (
          <ItemsBlock
            key={_id}
            title={title}
            centered
          >
            {categories.map(({
              _id: categoryId, name, bgImage, title: CategoryTitle,
            }) => (
              <GenreItem
                key={categoryId}
                wrapImageUrl={bgImage}
                title={CategoryTitle}
                name={name}
              />
            ))}
          </ItemsBlock>
        );
      })}
    </div>
  );
}));

export default Categories;
