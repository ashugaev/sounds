import React, { useEffect } from 'react';
import { inject, observer } from 'mobx-react';
import { useHistory, useParams } from 'react-router';
import query from '../../helpers/query';
import ItemsBlock from '../../components/ItemsBlockLayout';
import PageTracksList from '../../components/PageTracksList';
import Loader from '../../components/Loader';
import { capitalizeWords } from '../../../common/utils/capitalizeWords';

const AutoTag = inject('autoTagsTracksStore', 'autoTagListStore')(observer(({
  autoTagsTracksStore,
  autoTagListStore,
  className,
}) => {
  const {
    fetch,
    isTracksLoading,
    isTracksLoaded,
    noTracksToFetch,
    resetTracks,
  } = autoTagsTracksStore;

  const { autotagId } = useParams();

  useEffect(() => {
    fetch({ tagId: autotagId, resetTracks: true, rewrite: true });
    // autoTagListStore.byId(autotagId);

    return () => {
      resetTracks();
    };
  }, []);

  function lazyLoadingHandler() {
    fetch({
      afterObjId: autoTagsTracksStore.lastTrack._id,
    });
  }

  if (!isTracksLoaded || !autoTagListStore.loaded) {
    return (<Loader />);
  }

  let title = autoTagListStore.items[0] ? (`${autoTagListStore.items[0].text} Music`) : '';

  title = capitalizeWords(title);

  return (
    <div className={className}>
      {autotagId && autotagId.length && (
        <ItemsBlock
          title={title}
          tracks={autoTagsTracksStore.tracks}
          lazy
          isTracksLoading={isTracksLoading}
          noTracksToFetch={noTracksToFetch}
          lazyLoadingHandler={lazyLoadingHandler}
        >
          <PageTracksList
            tracksCustom={autoTagsTracksStore.tracks}
          />
        </ItemsBlock>
      )}
    </div>
  );
}));

export default AutoTag;
