const { startCronJob } = require('../helpers/startCronJob');
const { generateTags } = require('../scripts/generateTags/generateTags');

startCronJob({
  name: 'Generate Tags',
  callback: generateTags,
  // Once in a week at 12 pm
  period: '0 12 * * 1',
});
