const { escapeRegExp } = require('lodash');
const db = require('../../../server/schema/schema');
const { timer, timerAsync } = require('../../../common/utils/timer');
const { splitByChunks } = require('../../../common/utils/splitByChunks');

// This words can'n be alone in tag
const noMeaningWords = ['an', 'and', 'the',
  'for', 'and', 'nor', 'or', 'yet', 'so', 'in', 'a', 'hd', 'why', 'less', 'more', 'what'];
// Symbols for exclude
const trashSymbols = ['\\[', '\\]', '\\(', '\\)', '\\/', '\\,', '\\.', '\\-'];

const trashSymbolsRegExp = new RegExp(`[${trashSymbols.join('|')}]`, 'g');

// FIXME: Finish this steps cache logic
const STEP_DB_DATA = 'STEP_DB_DATA';

const JSON_CACHE_TEMPLATE = {
  step: null,
};

const normalizeString = (str) => {
  return str
  // No meaning symbols
    .replace(trashSymbolsRegExp, ' ')
  // Non alphabetic symbols
    .replace(/[^A-Z^a-z^\s^\d]*/g, '')
  // Many spaces to one
    .replace(/\s+/g, ' ')
    .toLowerCase();
};

/**
 * Automatic tags generator by Title
 *
 * TODO: Catch node async crash in cron wrapper util
 * TODO: Save locally data after each step for continue after restart script if it crased
 *
 * Безумная идея:
 * Для оптимизации поиска по строке перевести все слова в хэши один-два сивола
 *  и перед поиском по строке зашифровать еэ таким образом
 */
const generateTags = async () => {
  /**
   * - Get all tracks
   * - Iterate through tracks
   *      - Find every word in other titles
   *      - Exclude prepositions articles and etc
   *      - Remember indexed
   * - Join neighboring indexes and save separately in a different variations
   *      - Link single worlds which are used for this phrase
   * - Try to find similar phrases
   */

  console.log('generateTags started');

  const tracks = await timerAsync('Fetch tracks', () => db.Tracks.find({}).lean());

  const bigTitlesMegaString = timer(
    'Create big title string',
    () => {
      return tracks.map((track) => {
        track.snippet.title = normalizeString(track.snippet.title);
        return track.snippet.title;
      }).join(' ');
    },
  );

  const wordsArr = timer('Big str split', () => (
    bigTitlesMegaString.split(' ')
  ));

  const matchesObj = {};

  timer('Find matches', () => {
    for (let firstWordIndex = 0; firstWordIndex <= wordsArr.length; firstWordIndex++) {
      // TODO: Play with 'wordLength' value
      for (let wordLength = 0; wordLength <= 7; wordLength++) {
        const subArray = wordsArr.slice(firstWordIndex, wordLength + firstWordIndex);
        let phraseToSearch = subArray
          .join(' ')
          .trim();

        if (phraseToSearch.length < 4) {
          continue;
        }

        // numbers only
        if (/^\d+$/.test(phraseToSearch)) {
          continue;
        }

        if (noMeaningWords.includes(phraseToSearch)) {
          continue;
        }

        if (subArray.every(el => el.length < 3)) {
          continue;
        }

        // avoid regexp special characters
        phraseToSearch = escapeRegExp(phraseToSearch);

        if (matchesObj[phraseToSearch]) {
          continue;
        }

        const regexp = new RegExp(phraseToSearch, 'gi');

        matchesObj[phraseToSearch] = (bigTitlesMegaString.match(regexp) || []).length;
      }

      console.log('iteration', firstWordIndex, 'of', wordsArr.length);
    }
  });

  // Filter matches
  const goodMatchesObj = Object.keys(matchesObj).reduce((acc, el) => {
    const isGood = matchesObj[el] > 10;

    if (isGood) {
      acc[el] = matchesObj[el];
    }

    return acc;
  }, {});

  // TODO: Схлопнуть входящие друг в друга фразы с одинаковым кол-вом вхождений. Считать валидной самую длинную.

  console.log('matchesObj', goodMatchesObj);

  const autoTags = Object.keys(goodMatchesObj).map((el) => {
    return {
      text: el,
      count: goodMatchesObj[el],
      validatedManual: false,
      validatedAuto: false,
    };
  });

  const chunksTags = splitByChunks({ arr: autoTags, chunkSize: 100 });

  for (let i = 0; i < chunksTags.length; i++) {
    const chunk = chunksTags[i];

    console.log('iteration', i);

    // Update or create
    const updateAutotagsResult = await timerAsync(`Push tags to database ${i}`, () => db.AutoTags.bulkWrite(
      // Update existing and add new
      chunk.map(tag => ({
        updateOne: {
          // create if not exists
          upsert: true,
          filter: {
            text: tag.text,
          },
          update: tag,
        },
      })),
    ));

    console.log('updateAutotagsResult iteration', i, updateAutotagsResult);
  }


  // FIXME: ITERATE BY updatedAutoTagsIds AND USE TAG ID
  const autoTagTracks = autoTags.reduce((acc, autotag) => {
    // Check this tag int every elem
    const matchedTracks = tracks.filter((track) => {
      return track.snippet.title.includes(autotag.text);
    });

    matchedTracks.forEach((track) => {
      acc.push({
        trackId: track._id,
        autoTagId: autotag.id,
        autoTagText: autotag.text,
      });
    });

    return acc;
  }, []);

  console.log('autoTagTracks', autoTagTracks);

  const chunks = splitByChunks({ arr: autoTagTracks, chunkSize: 100 });

  for (let i = 0; i < chunks.length; i++) {
    const chunk = chunks[i];

    // Update or create
    const updateAutotagTracksResult = await timerAsync(`Push tracks to database ${i}`, () => db.AutoTagTracks.bulkWrite(
      // Update existing and add new
      chunk.map(tr => ({
        updateOne: {
          // create if not exists
          upsert: true,
          filter: {
            trackId: tr.trackId,
            autoTagText: tr.autoTagText,
          },
          update: tr,
        },
      })),
    ));

    console.log('updateAutotagTracks iteration', i, 'result', updateAutotagTracksResult);
  }

  console.log('FINISHED!');
};

module.exports = {
  generateTags,
};
