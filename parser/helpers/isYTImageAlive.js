const axios = require('axios');
const log4js = require('log4js');

const log = log4js.getLogger();
log.level = 'debug';

/**
 * Checking images status from YT
 */
const isYTImageAlive = async (url) => {
  try {
    await axios.get(url);
    log.debug('Image is Valid', url);

    return true;
  } catch (e) {
    const { response } = e;

    if (response && response.status === 404) {
      return false;
    }

    log.debug('Fetch image error', url);

    return true;
  }
};

module.exports = {
  isYTImageAlive,
};
