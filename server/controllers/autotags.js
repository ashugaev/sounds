const db = require('../schema/schema');

module.exports.all = async function (ctx) {
  const {
    fromObjId, afterObjId, beforeObjId, limit,
  } = ctx.query;

  const findParams = {};

  fromObjId && (findParams._id = { $gte: fromObjId });
  afterObjId && (findParams._id = { $gt: afterObjId });
  beforeObjId && (findParams._id = { $lt: beforeObjId }, sortParams._id = -1);

  try {
    ctx.body = await db.AutoTags.find(findParams).limit(limit || 80);
  } catch (e) {
    ctx.status = 500;
    ctx.body = e.message;
  }
};

module.exports.byId = async function (ctx) {
  const { id } = ctx.params;

  try {
    ctx.body = await db.AutoTags.find({ _id: id });
  } catch (e) {
    ctx.status = 500;
    ctx.body = e.message;
  }
};
