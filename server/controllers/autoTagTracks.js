const db = require('../schema/schema');

module.exports.byTagId = async function (ctx) {
  const {
    tagId,
    afterObjId,
    limit,
  } = ctx.query;

  const params = {
    _id: {},
  };

  if (afterObjId) {
    params._id.$gt = afterObjId;
  }

  if (!tagId) {
    ctx.status = 500;
    ctx.body = 'no tagId';
    return;
  }

  try {
    const tag = await db.AutoTags.find({ _id: tagId });

    if (!tag.length) {
      ctx.status = 500;
      ctx.body = 'invalid id';
      return;
    }

    const autoTagTracks = await db.AutoTagTracks.find({
      autoTagText: tag[0].text,
    });

    params._id.$in = autoTagTracks.map(track => track.trackId);

    const itemsLimit = Number(limit) || 40;

    const tracks = await db.Tracks.find(params).limit(itemsLimit);

    ctx.body = tracks;
  } catch (e) {
    ctx.status = 500;
    ctx.body = e.message;
  }
};
